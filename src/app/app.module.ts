import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
//import {MatListModule} from '@angular/material/list';
import { AppComponent } from './app.component';
import { TodoListComponent } from './component/todo-list/todo-list.component';
import {HttpClientModule} from "@angular/common/http";
import { RouterModule } from '@angular/router';
import { TodoListHeaderComponent } from './component/todo-list-header/todo-list-header.component';
import { TodoItemComponent } from './component/todo-item/todo-item.component';
import { TodoAppComponent } from './component/todo-app/todo-app.component';
import {environment} from "../environments/environment";
import {TodoRestInMemDataService} from "./service/todo-rest-in-mem-data.service";
import {InMemoryWebApiModule} from "angular-in-memory-web-api";


@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    TodoListHeaderComponent,
    TodoItemComponent,
    TodoAppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
  //  MatListModule,
    RouterModule,
    environment.production ?
    [] : InMemoryWebApiModule.forRoot(TodoRestInMemDataService)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
