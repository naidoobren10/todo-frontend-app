import { Component, OnInit } from '@angular/core';
import {TodoItem} from "../../model/todo/todoItem";
import {TodoAppServiceService} from "../../service/todo-app-service.service";

@Component({
  selector: 'app-todo-app',
  templateUrl: './todo-app.component.html',
  styleUrls: ['./todo-app.component.css']
})
export class TodoAppComponent implements OnInit {

  todoItems: TodoItem[] = [];
  completeTabSelected : boolean = false;
  todoTabSelected : boolean = false;

  constructor(private todoService: TodoAppServiceService) {
  }

  public ngOnInit(): void {
    this.todoService.retrieveAllTodoItems()
      .subscribe((todoItems) => {
        this.todoItems = todoItems;
      });
    this.completeTabSelected = false;
    this.todoTabSelected =false;
  }

  addTodoItem(todoItem: TodoItem) {
    this.todoService
      .addTodoItem(todoItem)
      .subscribe(
        (newTodoItem) => {
          this.todoItems = this.todoItems.concat(newTodoItem);
        });
  }

  deleteTodoItem(todoItem: TodoItem) {
    this.todoService
      .deleteTodoItemById(todoItem.id)
      .subscribe((
        (_) => {
          this.todoItems = this.todoItems.filter((t) => t.id !== todoItem.id)
        }
      ));
  }

  completeTodoItem(todoItem: TodoItem) {
    this.todoService
      .updateTodoItemStatus(todoItem)
      .subscribe((updatedTodoItem) => {
        todoItem = updatedTodoItem;
        if(!updatedTodoItem.description){
          this.todoItems = this.todoItems.filter((t) => t.id !== updatedTodoItem.id);
        }
        if(this.completeTabSelected && !updatedTodoItem.complete){
          this.todoItems = this.todoItems.filter((t) => t.id !== updatedTodoItem.id);
        }else if(this.todoTabSelected && updatedTodoItem.complete){
          this.todoItems = this.todoItems.filter((t) => t.id !== updatedTodoItem.id);
        }

      })
  }

  clearAllCompletedItems(){
    this.todoService
      .deleteCompletedTodoItems()
      .subscribe((todoItems) => {
        this.todoItems = todoItems;
      })
  }



  filterTodoList(flag: boolean, completeTabSelected:boolean, todoTabSelected:boolean) {
    this.completeTabSelected = completeTabSelected;
    this.todoTabSelected = todoTabSelected;
    this.todoService
      .retrieveTodoItemByStatus(flag)
      .subscribe((todoItems) => {
        this.todoItems = todoItems;
      })
      /*this.todoItems = this.cacheItems.filter((t) => t.complete === flag);*/
  }

  checkAllTodoItemAsComplete(todoItems: TodoItem[]){
     for(let todoItem of todoItems){
       this.completeTodoItem(todoItem)
       this.ngOnInit();
     }
  }

  showClearCompletedItemsButton(){
    for(let todoItem of this.todoItems){
      if(todoItem.complete){
        return true;
      }
    }
    return false;
  }

}
