import {Component, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';
import {TodoItem} from "../../model/todo/todoItem";

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class TodoItemComponent {
  editMode = false;

  constructor() { }


  @Input() todoItem: TodoItem;

  @Output()
  remove: EventEmitter<TodoItem> = new EventEmitter();

  @Output()
  toggleComplete: EventEmitter<TodoItem> = new EventEmitter();

 /* @Output()
  itemUpdated: EventEmitter<TodoItem> =  new EventEmitter();*/

  toggleTodoComplete(todoItem: TodoItem){
    todoItem.complete = !todoItem.complete;
    this.toggleComplete.emit({
      id: todoItem.id,
      description: todoItem.description,
      complete: todoItem.complete
    });
    this.todoItem.complete = todoItem.complete;
  }

  removeTodo(todoItem: TodoItem){
    this.remove.emit(todoItem);
  }
  
  enterEditMode(element: HTMLInputElement){
    this.editMode = true;
    if(this.editMode){
      setTimeout(() => {element.focus;}, 0);
    }
  }

  cancelEdit(element: HTMLInputElement){
    this.editMode = false;
    element.value = this.todoItem.description;
  }

  commitEdit(updatedText: string){
    this.editMode = false;
    this.todoItem.description = updatedText;
    this.toggleComplete.emit({
      id : this.todoItem.id,
      description : updatedText,
      complete : this.todoItem.complete
    })
  }

}
