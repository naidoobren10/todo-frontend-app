import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TodoItem} from "../../model/todo/todoItem";

@Component({
  selector: 'app-todo-list-header',
  templateUrl: './todo-list-header.component.html',
  styleUrls: ['./todo-list-header.component.css']
})
export class TodoListHeaderComponent {

  newTodoItem: TodoItem = new TodoItem();
  checkeditems: TodoItem[] = [];
  @Input()
  todoItems:TodoItem[];

  @Output()
  add: EventEmitter<TodoItem> = new EventEmitter()

  @Output()
  checkAllItems: EventEmitter<TodoItem[]> = new EventEmitter();

  constructor() { }

  addTodo(){
    if(this.newTodoItem.description){
      this.add.emit(this.newTodoItem);
      this.newTodoItem = new TodoItem()
    }
  }

  checkAllTodoItems(){
    for(let todoItem of this.todoItems){
      todoItem.complete = true;
      this.checkeditems.push(todoItem);
    }
    this.checkAllItems.emit(this.checkeditems);
  }

}
