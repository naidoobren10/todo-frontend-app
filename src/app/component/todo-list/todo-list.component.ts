import { Component,EventEmitter,Input,Output} from '@angular/core';
import { TodoItem} from "../../model/todo/todoItem";

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent{


  @Input()
  todoItems:TodoItem[];

  @Output()
  remove: EventEmitter<TodoItem> = new EventEmitter();

  @Output()
  completeItem: EventEmitter<TodoItem> = new EventEmitter();

  constructor() {  }

  completeTodo(todoItem: TodoItem){
    this.completeItem.emit(todoItem);
  }

  deleteTodo(todoItem: TodoItem){
    this.remove.emit(todoItem);
  }



}
