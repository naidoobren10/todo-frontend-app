import {TodoItem} from "./todoItem";

describe('TodoItem', ()=> {
  it('should create an instance', () =>{
    expect(new TodoItem()).toBeTruthy();
  });

  it('should accept values in the constructor', () => {
    let todoItem = new TodoItem({
      description: 'hello',
      complete: true
    });
    expect(todoItem.description).toEqual('hello');
    expect(todoItem.complete).toEqual(true);
  });
});
