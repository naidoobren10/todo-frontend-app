export class TodoItem {

  id: number;
  description: string;
  complete: boolean = false;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
