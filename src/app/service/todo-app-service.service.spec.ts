import { TestBed , async, inject} from '@angular/core/testing';

import { TodoAppServiceService } from './todo-app-service.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('TodoAppServiceService', () => {
  let service: TodoAppServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(TodoAppServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
