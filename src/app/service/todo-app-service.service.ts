import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable} from 'rxjs';
import { TodoItem} from "../model/todo/todoItem";
import {TodoRestControllerService} from "./todo-rest-controller.service";

@Injectable({
  providedIn: 'root'
})

export class TodoAppServiceService {

  constructor(private todoRestController: TodoRestControllerService) { }

  addTodoItem(todoItem: TodoItem): Observable<TodoItem> {
     return this.todoRestController.addTodoItem(todoItem);
  }

  retrieveAllTodoItems():Observable<TodoItem[]>{
     return this.todoRestController.retrieveAllTodoItems();
  }

  deleteTodoItemById(id: number): Observable<TodoItem>{
     return this.todoRestController.deleteTodoItem(id);
  }

  updateTodoItemStatus(todoItem: TodoItem){
      return this.todoRestController.updateTodoItem(todoItem);
  }

  retrieveTodoItemByStatus(status : boolean): Observable<TodoItem[]>{
     return this.todoRestController.retrieveTodoItemsByStatus(status);
  }

  deleteCompletedTodoItems(): Observable<TodoItem[]>{
    return this.todoRestController.deleteCompletedTodoItems();
  }
}
