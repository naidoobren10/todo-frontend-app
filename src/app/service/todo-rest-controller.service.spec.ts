import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule,HttpTestingController } from '@angular/common/http/testing';
import { TodoRestControllerService } from './todo-rest-controller.service';

describe('TodoRestControllerService', () => {
  let service: TodoRestControllerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(TodoRestControllerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
