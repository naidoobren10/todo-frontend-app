import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from "rxjs";
import {TodoItem} from "../model/todo/todoItem";




@Injectable({
  providedIn: 'root'
})
export class TodoRestControllerService {

  private todoBackEndUrl = environment.todoBackendURL;

  constructor(private http: HttpClient) { }

  retrieveAllTodoItems():Observable<TodoItem[]> {
    return this.http.get<TodoItem[]>(this.todoBackEndUrl + '/getAllTodoItems')
  }

  public addTodoItem(todoItem: TodoItem): Observable<TodoItem> {
    return this.http.post<TodoItem>(this.todoBackEndUrl+'/addItem', todoItem);
  }

  public deleteTodoItem(id: number): Observable<TodoItem> {
    return this.http.delete<TodoItem>(this.todoBackEndUrl+'/deleteTodoItem/' + id);
  }

  public updateTodoItem(todoItem: TodoItem): Observable<TodoItem> {
    return this.http.post<TodoItem>(this.todoBackEndUrl+'/updateTodoItem',todoItem);
  }

  public retrieveTodoItemsByStatus(status: boolean) : Observable<TodoItem[]> {
    return this.http.get<TodoItem[]>(this.todoBackEndUrl + '/getTodoItemsByStatus/' + status)

  }

  public deleteCompletedTodoItems() : Observable<TodoItem[]> {
    return this.http.delete<TodoItem[]>(this.todoBackEndUrl+'/deleteCompletedTodoItems');
  }
}
