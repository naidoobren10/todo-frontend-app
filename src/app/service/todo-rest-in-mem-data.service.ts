import { Injectable } from '@angular/core';
import {InMemoryDbService} from "angular-in-memory-web-api";
import {TodoItem} from "../model/todo/todoItem";

@Injectable({
  providedIn: 'root'
})
export class TodoRestInMemDataService  implements InMemoryDbService{
  constructor() { }

  createDb(){
    let todoItems: TodoItem[] = [
      {id: 1, description:"todoItem1", complete:true},
      {id: 1, description:"todoItem2", complete:false},
      {id: 1, description:"todoItem3", complete:true},
      {id: 1, description:"todoItem4", complete:false},
      {id: 1, description:"todoItem5", complete:true},
      {id: 1, description:"todoItem6", complete:true},
      {id: 1, description:"todoItem7", complete:false}
    ];   return {todoItems};
  }

}
