export const environment = {
  production: true,
  todoBackendURL: 'https://todo-backend-app-27.herokuapp.com/todo'
};
